#!/usr/bin/env python

import RPi.GPIO as GPIO
import time
import sys
import os
from subprocess import call

BUTTON = 26


def setup():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)    
    GPIO.setup(BUTTON, GPIO.IN)


def destroy():
    GPIO.cleanup()


def loop():
    time.sleep(1)
    while True:
        if GPIO.input(BUTTON) == GPIO.LOW:
            call("/home/pi/photos/facial-korzakov.sh", shell=True)


if __name__ == "__main__":
    setup()
    try:
        loop()
    except KeyboardInterrupt:
        destroy()
