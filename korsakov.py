#!/usr/bin/env python

import os
import sys
import time

import RPi.GPIO as GPIO

TRIG = 17
ECHO = 18
SWITCH = 8
DIRECTION = 7
target_dist = 100  # Default to 100 if not provided by commandline


def setup():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(TRIG, GPIO.OUT)
    GPIO.setup(ECHO, GPIO.IN)
    GPIO.setup(SWITCH, GPIO.OUT)
    GPIO.setup(DIRECTION, GPIO.OUT)


def up():
    GPIO.output(DIRECTION, 1)
    GPIO.output(SWITCH, 1)
    while True:
        if in_range(0.1):
            GPIO.output(SWITCH, 0)
            break


def down():
    GPIO.output(DIRECTION, 0)
    GPIO.output(SWITCH, 1)
    while True:
        if in_range(0.1):
            GPIO.output(SWITCH, 0)
            break


def distance():
    GPIO.output(TRIG, 0)
    time.sleep(0.000002)

    GPIO.output(TRIG, 1)
    time.sleep(0.00001)
    GPIO.output(TRIG, 0)

    while GPIO.input(ECHO) == 0:
        a = 0
    time1 = time.time()
    while GPIO.input(ECHO) == 1:
        a = 1
    time2 = time.time()

    during = time2 - time1
    return during * 340 / 2 * 100


def go_up():
    return target_dist > distance()


def in_range(marge):
    return (target_dist - marge) <= distance() <= (target_dist + marge)


def loop():
    print("current dist")
    print(distance())
    if not in_range(1):
        if go_up():
            print('going up')
            up()
        else:
            print('going down')
            down()
        print('Destination reached at:')
        print(distance())
        proc = "python publish.py -s " + str(distance())
        # subprocess.call(proc, shell=True)
        os.system(proc)
    else:
        print("Already at destination")



def destroy():
    GPIO.cleanup()


if __name__ == "__main__":
    setup()
    try:
        if (len(sys.argv) > 1):
            target_dist = int(sys.argv[1])
        loop()
    except KeyboardInterrupt:
        destroy()
